<?php

function biblio_migrate_show_postprocess() {
  
  return drupal_get_form('biblio_migrate_postprocess_form');
  
}

// ---------------------------------------------------------

function biblio_migrate_postprocess_form_submit($form, &$form_state) {
  
  $option = $form_state['values']['options'];
  
  //delete?
  if ($option == 'delete') {

    //Get all IDs in the system for biblio records
    $all_biblio_ids = array();
    $result = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => 'biblio'));
    foreach ($result as $r) {
      $all_biblio_ids[] = $r->nid;
    }
      
    //Get all IDs of imported biblio records
    $imported_recs = variable_get('biblio_migrate_batch_import', FALSE);      

    //To Delete
    $to_delete = array_diff($all_biblio_ids, array_keys($imported_recs));
    
    node_delete_multiple($to_delete);
    
    drupal_set_message(t("Deleted @count biblio nodes", array('@count' => count($to_delete)))); 
  }
  
  //We're going to remove the persistent data either way..
  variable_set('biblio_migrate_batch_import', array());
  drupal_set_message(t("Cleared persistent import data!  The cache is clean!"));
  
  drupal_flush_all_caches();
  drupal_goto('biblio/migrate');
}

// ---------------------------------------------------------

/*function _biblio_migrate_deleteold_batch_finish($success, $result, $operations) {
  
  drupal_set_message("Succesfully deleted " . count($result) . " old biblio records.");
  drupal_flush_all_caches();
}

// ---------------------------------------------------------

function _biblio_migrate_deleteold_batch_process(&$context) {
  
  //How many records shall we process at a time?
  $num_records = 5;
    
  //First run? - Setup some information
  if ( ! isset($context['results']) == 0)
  {
    //Get all IDs in the system for biblio records
    $all_biblio_ids = array();
    $result = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => 'biblio'));
    foreach ($result as $r) {
      $all_biblio_ids[] = $r->nid;
    }
      
    //Get all IDs of imported biblio records
    $imported_recs = variable_get('biblio_migrate_batch_import', FALSE);    
        
    $context['sandbox']['old_recs'] = array_diff($all_biblio_ids, array_keys($imported_recs));     
    $context['sandbox']['total_old_recs'] = count($to_delete);
    $context['sandbox']['times'] = array();
    $context['sandbox']['average_delete_time'] = 'calculating..';
  }
  
  //Set message
  $msg  = "Deleting old biblio items: " . count($context['sandbox']['old_recs']) ." remaining";
  $msg .= " (Average delete time per record: ". $context['sandbox']['average_delete_time'] .")";

  $currct = 0;
  while($goner = array_shift($context['sandbox']['old_recs']) && $currct < $num_records) {
    
    $node_del_start = time();
    node_delete($goner);
    
    //Update statuses
    $context['sandbox']['times'][] = time() - $node_del_start;
    $context['results'][] = $goner;
    $context['finished'] = count($context['results']) / $context['sandbox']['total_old_recs'];
    
    $currct++;
  }
  
  $context['sandbox']['average_delete_time'] = array_sum($context['sandbox']['times']) / count($context['sandbox']['times']);
}*/

// ---------------------------------------------------------

function biblio_migrate_postprocess_form($form, &$form_state) {
  
  $data = variable_get('biblio_migrate_batch_import', array());
  $counts = biblio_migrate_import_get_counts($data);
  
  //Form header
  $msg = "<h3>" . t('Post Process Import Biblio') . "</h3>";
  $msg .= "<p>" . t('You recently imported <strong>@num</strong> bibliography records.', array('@num' => $counts['added'])) . "</p>";
  $form['header'] = array(
    '#markup' => $msg
  );
  
  $form['action'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select an Action')
  );
  
  $action_options = array(
    'delete' => t('Delete all old records'),
    'remove' => t('Just remove persistent import data')
  );
  
  $form['action']['options'] = array(
    '#type' => 'radios',
    '#title' => 'Choose an action',
    '#required' => TRUE,
    '#options' => $action_options,
    '#description' => t("What to do.. what to do..")
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Post-Process It!')
  );

  return $form;
}

/* EOF: biblio_migrate_postprocess.inc */