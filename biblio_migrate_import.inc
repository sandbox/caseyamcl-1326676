<?php

/**
 * @file bilbio_migrate_import.inc
 * Import functionality for Biblio Migrate
 * 
 */

// ---------------------------------------------------------

/**
 * Show the biblio migrate import page
 *
 * This is the page callback function for the hook_menu() callback.  It
 * decided what to do based on the arguments sent to it via the path.
 *
 * @return mixed
 * Returns the import form
 */
function biblio_migrate_show_import() {

  if ( ! user_access(BIBLIO_MIGRATE_PERM))
  {
    drupal_access_denied();
  }
  
  return drupal_get_form('biblio_migrate_import_form');
}

// ---------------------------------------------------------

/**
 * Implements hook_form_validate().
 */
function biblio_migrate_import_form_validate($form, &$form_state) {
  
  $ol = $form_state['values']['import_limit'];  
  if ( ! is_numeric($ol) OR ((string) (int) $ol != (string) $ol) OR ((int) $ol < 0) OR (int) $ol % 1 !== 0) {
    form_set_error('import_limit', t("The import limit must be a positive integer!"));
  }

}

// ---------------------------------------------------------

function biblio_migrate_import_form_submit($form, &$form_state) {
  
  //Get values from the form
  
  //Reset persistent data (if we are not resuming a previous run)
  //@TODO: Make this logic persistent!
  variable_set('biblio_migrate_batch_import', array());
  
  //Get the value for which mode to use and limit
  $ol = $form_state['values']['import_limit'];  
  $input_file = file_load($form_state['values']['input_file']);
  
  $batch = array(
    'title' => t('Migrate Import Biblio'),
    'operations' => array(
      array('_biblio_migrate_batch_process', array($ol, $input_file))
    ),
    'finished' => '_biblio_migrate_batch_finish',
  );
  
  batch_set($batch);
}

// ---------------------------------------------------------

function _biblio_migrate_batch_finish($success, $results, $operations) {
  
  //Flush cache - Necessary step!!
  drupal_flush_all_caches();
    
  //Set notices
  if ( ! $success) {
    drupal_set_message("Looks like there was  PHP error during the import", 'warning');
  }
  
  //Generate the counts
  $counts = biblio_migrate_import_get_counts($results);
  
  $msgs[] = t("Finished the import!");
  $msgs[] = t("Imported @num records", array('@num' => $counts['added']));
  $msgs[] = t("Updated @num records", array('@num' => $counts['updated']));
  $msgs[] = t("Deleted @num existing records", array('@num' => $counts['deleted']));
  $msgs[] = t("There were @num records with JSON decode errors.", array('@num' => $counts['error']));
  
  if ($counts['tested'] > 0)
    $msgs[] = t("There were @num marked as tested (not actually imported)", array('@num' => $counts['tested']));
  
  foreach($msgs as $msg) {
    drupal_set_message(check_plain($msg));
  }
}

// ---------------------------------------------------------

function _biblio_migrate_batch_process($limit_records, $input_file, &$context) {

  //Permanent Persistence tracking
  $tracking = variable_get('biblio_migrate_batch_import', array());
  
  //How many records shall we process at a time?
  $num_records = 10;
  
  //Input file
  $filename = drupal_realpath($input_file->uri); 
  
  //First run? - Setup some information
  if ( ! isset($context['sandbox']['curr_file_line']))
  {   
    $context['sandbox']['curr_file_line'] = 0;
    $context['sandbox']['total_file_lines'] = _get_file_linecount($filename);
    $context['sandbox']['times'][] = array();
    $context['sandbox']['average_rec_import_time'] = 'calculating';
   
    //If we're limiting records, and the records are less than the file lines
    if ($limit_records > 0 && $limit_records < $context['sandbox']['total_file_lines'])
      $context['sandbox']['num_recs_to_import'] = $limit_records;
    else
      $context['sandbox']['num_recs_to_import'] = $context['sandbox']['total_file_lines'];
  }
  
  //End record for this go-around
  $end_num = ($context['sandbox']['curr_file_line'] + $num_records <= $context['sandbox']['num_recs_to_import']) ? $context['sandbox']['curr_file_line'] + $num_records : $context['sandbox']['num_recs_to_import'];  

  //Current message
  $curr_msg  = "Importing records ". ($context['sandbox']['curr_file_line'] + 1) ." thru {$end_num} of {$context['sandbox']['num_recs_to_import']} records (selected from a total file count of {$context['sandbox']['total_file_lines']} records";
  $curr_msg .= " (Average Record Import Time: ". $context['sandbox']['average_rec_import_time'] .")";
  $context['message'] = t($curr_msg);
   
  //Open the file
  $input_fh = fopen($filename, 'r');
    
  //Seek to the line we want to be on
  //@TODO: Change the logic so that it uses file pointer location instead!
  if ($context['sandbox']['curr_file_line'] > 0) {    
    for($i = 0; $i < $context['sandbox']['curr_file_line']; $i++)
      fgets($input_fh);
  }
  
  //Go through num_records and import the records
  //@TODO: Problem? Porbably the while logic here...
  while($context['sandbox']['curr_file_line'] < $end_num && ! feof($input_fh)) {
    
    //If we're done...
    if (count($context['results']) == $context['sandbox']['num_recs_to_import']) {
      $context['finished'] = 1;
      break;      
    }
        
    //Import a single record
    if ($line = trim(fgets($input_fh))) {
         
      if ($input_record = json_decode($line, TRUE)) {
      
        $rec_start = time();
        $incoming_node_id = $input_record['nid'];
       
        //The MaGiC Happens Here! - Do the import
        $new_node_id = _biblio_migrate_import_do_import_node($input_record);
        
        $curr_result = ($new_node_id) ? 'added' : 'failed';
         
        //Calculate the time it took
        $context['sandbox']['times'][] = time() - $rec_start;
      }
      else
        $curr_result = "error";
    }   
    
    //Clear out memory (probably not necessary)
    unset($input_record);
    
    //Formulate a result line
    $result_line = array();
    $result_line['file_line'] = $context['sandbox']['curr_file_line'];
    $result_line['line_status'] = $curr_result;
    $result_line['incoming_node'] = (isset($incoming_node_id)) ? $incoming_node_id : NULL;
    $result_line['new_node'] = (isset($new_node_id) && $new_node_id) ? $new_node_id : NULL;
    
    //Output a result line
    $context['results'][] = $result_line;
    $context['sandbox']['curr_file_line']++;
    $context['finished'] = count($context['results']) / $context['sandbox']['num_recs_to_import'];    
    
    //Update persistent tracking
    if (isset($new_node_id))
      $tracking[$new_node_id] = $result_line;
  }
  
  //Another fallback for if we're at the end of the file
  if (feof($input_fh) OR count($context['results']) == $context['sandbox']['num_records_to_import']) {
      $context['finished'] = 1;
      break;
  }    
  //Close the file
  fclose($input_fh);
  
  //Update the average time
  $context['sandbox']['average_rec_import_time'] = array_sum($context['sandbox']['times']) / count($context['sandbox']['times']);
  
  //Write persistent tracking
  variable_set('biblio_migrate_batch_import', $tracking);
}

// ---------------------------------------------------------

function biblio_migrate_import_form($form, &$form_state) {
  
  //Form header
  $msg = "<h3>" . t('Import Biblio') . "</h3>";
  $msg .= "<p>" . t('Choose a valid JSON file that was exported by this interface on this or another Drupal site') . "</p>";
  $form['header'] = array(
    '#markup' => $msg
  );
    
  //Upload filesize warning
  $msg = "<p>" . t('The maximum filesize that you can upload to this server is:') . ' <strong>' . format_size(file_upload_max_size()) . "</strong></p>";
  $form['filesize_msg'] = array(
    '#markup' => $msg
  );

  //Import file
  $form['input_file'] = array(
    '#name' => 'files[import_file]',
    '#type' => 'managed_file',
    '#title' => t('Import file'),
    '#upload_location' => 'public://tmp_biblio_import/',
    '#progress_indicator' => 'bar',
    '#required' => TRUE
  );
    
  $form['import_limit'] = array(
    '#type' => 'textfield',
    '#title' => t("Limit import records"),
    '#description' => t('Limit input records (mainly used for testing).  0 is Unlimited'),
    '#required' => TRUE,
    '#default_value' => 0
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import It!')
  );

  return $form;
}

// ---------------------------------------------------------

/**
 * Biblio Migrate Import Process Attatchment
 * 
 * Some black magic to re-add file attatchments to nodes
 * 
 * @param type $attach_info
 * @param type $nid
 * @param type $vid
 * @return type 
 */
function _biblio_migrate_import_process_attachment($attach_info, $nid, $vid) {
  
  $debug_msg[] = "File Attatchment Import for: $nid (version $vid)";

  //Does the $attach_info->uri exist in the database already?
    //If so, find the corresponding record attachment join tables, and delete them (if they exist)

  //No?
    //Run file_save to add it to the database

  //Create a record attatchment join tables, and update the node id
  $q = db_query("SELECT fid, uri FROM {file_managed} WHERE uri = :uri", array(':uri' => $attach_info['uri']));
    
  //Row exists in the database?
  if ($q->rowCount() > 0) {
    $fid = $q->fetchObject()->fid;
    $debug_msg[] = "Found file with uri {$attach_info['uri']} in the database.  Use existing fid: $fid"; 
  } else {
    unset($attach_info['fid']);
    unset($attach_info['uid']);
    $fid = file_save((object) $attach_info)->fid;
    $debug_msg[] = "No file with uri {$attach_info['uri']} in the database.  Created fid: $fid"; 
  }

  //Join tables for old fid exist?
  $q = db_query("SELECT * FROM {field_data_field_attatchment} WHERE field_attatchment_fid = :fid", array(':fid' => $fid));

  //Delete the old rows for the file attatchment
  if ($q->rowCount() > 0) {
    $num_deleted = db_delete('field_data_field_attatchment')->condition('field_attatchment_fid', $fid)->execute();
    $num_deleted = db_delete('field_revision_field_attatchment')->condition('field_attatchment_fid', $fid)->execute();
    $debug_msg[] = "Deleted existing file relationships for file with id $fid from the join tables";
  }

  //Add the new rows 
  $fields = array(
    'entity_type' => 'node',
    'bundle' => 'biblio',
    'entity_id' => $nid,
    'revision_id' => $vid,
    'language' => 'und',
    'delta' => 0,
    'field_attatchment_fid' => $fid
   );

  //Insert values
  db_insert('field_data_field_attatchment')->fields($fields)->execute();
  db_insert('field_revision_field_attatchment')->fields($fields)->execute();
  $debug_msg[] = "Created new file relationships for file with id $fid, node_id $nid, and version_id $vid";

  //drupal_set_message(implode("<br/>", $debug_msg));

  return TRUE;
}

// ---------------------------------------------------------

/**
 * Prepare JSON biblio object for import into Drupal
 *
 * If this weren't hacked together, we would want to get the comments,
 * and we would definitely also want to read each custom field and see if
 * the field structure exists before attempting to import it
 * 
 * @param array $input_node
 * The raw JSON field from the original output
 * 
 * @return array
 * Something we can actually insert or update in the database
 */
function _biblio_migrate_import_do_import_node($input_node) {

  //PREPARE THE NODE
  
  //Attatchment information?  Prepare to import that..
  if (isset($input_node['field_attatchment']['und'][0]) && ! empty($input_node['field_attatchment']['und'][0])) {
    $attach_info = $input_node['field_attatchment']['und'][0];
  }
    
  //Unset things we need to unset from the JSON object, because they will
  //be recreated automatically
  $to_unset = array(
    'nid', 'vid', 'cid', 'uid', 'uuid', 'log', //this will be managed automatically
    'tnid', 'created', 'changed', //ditto
    'revision_timestamp', 'revision_uid', //new node or existing, so kill the old stuff
    'comment', 'last_comment_timestamp', 'last_comment_name', 'last_comment_uid', 'comment_count', //unset comment stuff for now
    'rdf_mapping' //this ought to be regenerated automatically
  );

  foreach($to_unset as $us) {
    if (isset($input_node[$us]))
      unset($input_node[$us]);
  }
  
 
  //Remove identifiers from biblio_contributors
  foreach ($input_node['biblio_contributors'] as &$contrib) {    
    unset($contrib['nid']);
    unset($contrib['vid']);
    unset($contrib['cid']);
    unset($contrib['drupal_uid']);
    unset($contrib['md5']);
  }

  //SAVE THE NODE!!!!
  $node = (object) $input_node;
  node_object_prepare($node);
  node_submit($node);
  node_save($node);
  
  //Fix up the file attatchment
  if (isset($node->nid) && isset($attach_info))
    _biblio_migrate_import_process_attachment($attach_info, $node->nid, $node->vid);
    
  //Return the new node id or FALSE if faield
  return (isset($node->nid)) ? $node->nid : FALSE;
  
}

// ---------------------------------------------------------

/**
 * Helper function to get the linecount for a file
 * 
 * @param string $filename
 * The name of the file for which to count the lines
 * 
 * @return int 
 * The number of lines
 */
function _get_file_linecount($filename) {
  
  $linecount = 0;
  $handle = fopen($filename, "r");

  while(!feof($handle)){
    $line = trim(fgets($handle));
    
    if ($line != '')
      $linecount++;
  }

  fclose($handle);

  return $linecount;
}

/* EOF: biblio_migrate_import.inc */