<?php

// ---------------------------------------------------------

/**
 * Show the biblio migrate export page
 *
 * This is the page callback function for the hook_menu() callback.  It
 * decided what to do based on the arguments sent to it via the path.
 *
 * @param string $action
 * The action to peform.  Legal values are 'download' or 'delete'.
 *
 * @param int $file
 * The biblio migrate file id for which to perform the desired action
 *
 * @return mixed
 * Simply calls another function, based on the desired actino
 */
function biblio_migrate_show_export($action = NULL, $file_id = NULL) {

  if ( ! user_access(BIBLIO_MIGRATE_PERM))
  {
    drupal_access_denied();
  }
  
  switch ($action) {

    case 'download':
      return _biblio_migrate_download_file($file_id); break;
    case 'delete':
      return _biblio_migrate_delete_file($file_id); break;
    default:
      return drupal_get_form('biblio_migrate_export_form');
  }
}

// ---------------------------------------------------------

/**
 * Download the specified biblio migrate export file
 *
 * Downloads the desired export file; shows the form with an error message
 * if there was an error.
 *
 * @param int $file_id
 * @return void
 */
function _biblio_migrate_download_file($file_id) {

  $files = variable_get('biblio_migrate_existing_files', array());

  if ( ! isset($files[$file_id])) {
    drupal_set_message(check_plain(t("Could ndot identify the biblio migrate export file with ID:") . " $file_id"), 'error');
    return drupal_get_form('biblio_migrate_export_form');
  }

  if ( ! is_readable($files[$file_id])) {
    drupal_set_message(check_plain(t("Error downloading " . $files[$file_id] . " from the sever.  Could not read the file (permissions?).")), 'error');
    return drupal_get_form('biblio_migrate_export_form');
  }

  //Couldn't figure out the 'drupal way' to do this due to incomplete
  //documentation, so I made a quick hack
  header("Content-type: application/json");
  header('Content-Disposition: attachment; filename="' . basename($files[$file_id]) . '"');
  echo file_get_contents($files[$file_id]);
}

// ---------------------------------------------------------

/**
 * Delete the specified biblio migrate export file
 *
 * Deletes the file from the server; shows the form with an error message
 * if there was an error
 *
 * @param int $file_id
 * @return void
 */
function _biblio_migrate_delete_file($file_id) {

  $files = variable_get('biblio_migrate_existing_files', array());

  if ( ! isset($files[$file_id])) {
    drupal_set_message(check_plain(t("Could ndot identify the biblio migrate export file with ID:") . " $file_id"), 'error');
    return drupal_get_form('biblio_migrate_export_form');
  }

  if ( ! is_readable($files[$file_id])) {
    drupal_set_message(check_plain(t("Error downloading " . $files[$file_id] . " from the sever.  Could not read the file (permissions?).")), 'error');
    return drupal_get_form('biblio_migrate_export_form');
  }

  if (unlink($files[$file_id]))
    drupal_set_message(check_plain(t("Succesfully deleted the file:")) . ' ' . $files[$file_id]);
  else
    drupal_set_message(check_plain(t("Error deleting the file:")) . ' ' . $files[$file_id], 'error');

  return drupal_get_form('biblio_migrate_export_form');
}

// ---------------------------------------------------------

/**
 * Implements hook_form_validate().
 */
function biblio_migrate_export_form_validate($form, &$form_state) {

  if ( ! preg_match("/^([a-zA-Z0-9_\-\.]+?)$/", $form_state['values']['output_filename'])) {
    form_set_error('output_filename', t("The output filename can only contain letters, numbers, underscores, dashes, or periods."));
  }
  
  $ol = $form_state['values']['output_limit'];  
  if ( ! is_numeric($ol) OR ((string) (int) $ol != (string) $ol) OR ((int) $ol < 0) OR (int) $ol % 1 !== 0) {
    form_set_error('output_limit', t("The output limit must be a positive integer (or zero)!"));
  }

  $oo = $form_state['values']['output_offset']; 
  if ( ! is_numeric($oo) OR ((string) (int) $oo != (string) $oo) OR ((int) $oo < 0) OR (int) $oo % 1 !== 0) {
    form_set_error('output_offset', t("The output offset must be a positive integer (or zero)!"));
  }
  
  
  if ( ! empty($form_state['values']['output_specific_nodes'])) {
    
    if ( ! preg_match("/^[0-9, ]+?$/", $form_state['values']['output_limit'])) {
      form_set_error('output_specific_nodes', t("Invalid input for output specific nodes!"));
    }
  }

}

// ---------------------------------------------------------

/**
 * Implements hook_form_submit().
 */
function biblio_migrate_export_form_submit($form, &$form_state) {

  $fname = $form_state['values']['output_filename'];
  $ol = $form_state['values']['output_limit'];
  $oo = $form_state['values']['output_offset']; 
  
  //Specific nodes?
  if ( ! empty($form_state['values']['output_specific_nodes'])) {
    $specific_nodes = array_map('trim', explode(',', $form_state['values']['output_specific_nodes']));
    $specific_nodes = array_filter($specific_nodes); //remove empties
  }
  
  //Perform the operation...
  ini_set('max_execution_time', 0);

  //Setup the output file
  $out_file = file_directory_temp() . DIRECTORY_SEPARATOR . $fname;
  $out_fh = fopen($out_file, 'w');
  
  //Establish a count
  $lcount = -1; //janky
  $count = 0;

  //Get em from the database
  $result = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => 'biblio'));
  
  foreach ($result as $r) {
    
    $lcount++;
    
    //Janky, but works -- Make this part of the query conditions instead!
    if ($oo > 0 && $lcount < $oo)
      continue;
    
    //Janky, but works -- Make this part of the query conditions instead!
    if ($ol > 0 && $count == $ol)
      break;
    
    if ( ! isset($specific_nodes) OR in_array($r->nid, $specific_nodes)) {
      $node = node_load($r->nid);
      fwrite($out_fh, json_encode($node) . "\n");
    
      $count++;
      unset($node);
    }
  }

  fclose($out_fh);
  
  //Set a variable with the output file
  $existing_files = variable_get('biblio_migrate_existing_files', array());
  variable_set('biblio_migrate_existing_files', array_unique(array_merge($existing_files, array($out_file))));

  //Set the output message
  drupal_set_message(check_plain(t("Outputted " . $count . " biblio nodes to JSON file:") . " $out_file"));

  //For now, report on the amount of memory used during that operation
  //drupal_set_message(check_plain(t("Memory used: " . number_format(memory_get_peak_usage() / 1048576, 2) . " mb")));



}

// ---------------------------------------------------------

/**
 * Determine which output files actually exist
 *
 * If a file doesn't exist, but is in the output files variable,
 * then remove it.
 *
 * @return array
 * A list of valid, existing, readble output files
 */
function _biblio_migrate_get_existing_output_files() {
  //Get the list
  $files = variable_get('biblio_migrate_existing_files', array());

  if (empty($files))
    return FALSE;

  $out_list = array();
  foreach ($files as $file) {
    if (is_readable($file)) {
      $out_list[] = $file;
    }
  }

  //Reset the variable, using only valid (still existing) files
  variable_set('biblio_migrate_existing_files', $out_list);

  return ( ! empty($out_list)) ? $out_list : FALSE;
}

// ---------------------------------------------------------

/**
 * Generate a display message for listing existing download files
 *
 * @param string $file
 * The full path to the file
 *
 * @return string
 * A friendly message displaying the filename, size, and create date
 */
function _biblio_migrate_generate_display_msg_for_file($file) {

  $fileinfo = stat($file);
  return basename($file) . ': (' . format_size($fileinfo['size']) . ') - Created (' . format_date($fileinfo['ctime']) . ')';
}

// ---------------------------------------------------------

/**
 * Implements hook_form().
 */
function biblio_migrate_export_form($form, &$form_state) {

  //Exeuction time warning
  $orig_exec_time = ini_get('max_execution_time');
  if ( ! ini_set('max_execution_time', 0)) {
    $msg = "Drupal was unable to reset the maximum execution time for
        a script on the server.  If you have a large collection, this action may
        fail.  The script will timeout after $orig_exec_time seconds";

    drupal_set_message(check_plain(t($msg)), 'warning');
  }
  else
    ini_set('max_execution_time', $orig_exec_time);


    $form['header'] = array(
      '#markup' => "<h3>" . t('Export Biblio') . "</h3>"
    );  
  
  //Existing file list
  if ($existing_files = _biblio_migrate_get_existing_output_files()) {
    foreach ($existing_files as $k => &$fn) {
      $filename = $fn;
      $fn = l(_biblio_migrate_generate_display_msg_for_file($fn), 'biblio/migrate/download/' . $k);

      if (is_writable($filename))
        $fn .= " " . l("(" . t('Delete') . ")", 'biblio/migrate/delete/' . $k);
    }

    $markup = "<p>There are " . count($existing_files) . " export files ready to download!</p><ul><li>" . implode('</li><li>', $existing_files) . "</li></ul>";

    $form['existing_downloads'] = array(
      '#markup' => $markup
    );
  }

  //Actual input items
  $form['output_filename'] = array(
    '#type' => 'textfield',
    '#title' => t('Output filename'),
    '#description' => t('The name of the XML file to export to'),
    '#required' => TRUE,
    '#default_value' => 'biblio_output.txt'
  ); 
    
  $form['output_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit output'),
    '#description' => t('Limit output.  0 means unlimited'),
    '#required' => TRUE,
    '#default_value' => 0
  );
  
  $form['output_offset'] = array(
    '#type' => 'textfield',
    '#title' => t('Output offset'),
    '#description' => t('Output offset is for breaking up large sets into small batches'),
    '#required' => TRUE,
    '#default_value' => 0
  );
  
  
  $form['output_specific_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Limit output to specific nodes'),
    '#description' => t('Specify specific nodes to output (separate node IDs by comma).'),
    '#required' => FALSE,
    '#default_value' => ''
  );  
  
  $ajax_settings['progress']['type'] = 'throbber';
  $ajax_settings['progress']['message'] = 'This may take a few minutes. . .';
  $ajax_settings['effect'] = 'slide';
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export It!'),
    '#ajax' => $ajax_settings
  );

  return $form;

}

/* EOF: biblio_migrate_export.inc */