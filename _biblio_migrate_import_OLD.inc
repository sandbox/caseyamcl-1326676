<?php

// ---------------------------------------------------------

/**
 * Show the biblio migrate import page
 *
 * This is the page callback function for the hook_menu() callback.  It
 * decided what to do based on the arguments sent to it via the path.
 *
 * @param string $action
 * The action to peform.  Legal values are 'download' or 'delete'.
 *
 * @param int $file
 * The biblio migrate file id for which to perform the desired action
 *
 * @return mixed
 * Simply calls another function, based on the desired actino
 */
function biblio_migrate_show_import($action = NULL, $file_id = NULL) {

  if ( ! user_access(BIBLIO_MIGRATE_PERM))
  {
    drupal_access_denied();
  }
  
  return drupal_get_form('biblio_migrate_import_form');
}

// ---------------------------------------------------------

/**
 * Implements hook_form_validate().
 */
function biblio_migrate_import_form_validate($form, &$form_state) {
  
  $ol = $form_state['values']['import_limit'];  
  if ( ! is_numeric($ol) OR ((string) (int) $ol != (string) $ol) OR ((int) $ol < 0) OR (int) $ol % 1 !== 0) {
    form_set_error('import_limit', t("The import limit must be a positive integer!"));
  }

}
// ---------------------------------------------------------

function biblio_migrate_import_form_submit($form, &$form_state) {
  
  //Get values from the form
  
  //Get the value for which mode to use and limit
  $import_mode = $form_state['values']['import_mode'];
  $ol = $form_state['values']['import_limit'];  
  $input_file = file_load($form_state['values']['input_file']);
  
  $batch = array(
    'title' => t('Migrate Import Biblio'),
    'operations' => array(
      array('_biblio_migrate_batch_process', array($import_mode, $ol, $input_file))
    ),
    'finished' => '_biblio_migrate_batch_finish',
    'file' => __FILE__
  );
  
  batch_set($batch);
}



function _biblio_migrate_batch_finish($success, $results, $operations) {
  
  $out_message = array("Import Finished.");
  $out_message[] = "Deleted " . $context['sandbox']['counts']->deleted . 'existing records.';
  $out_message[] = "Added " . $context['sandbox']['counts']->added . " new records.";
  $out_message[] = "Updated " . $context['sandbox']['counts']->updated . " records.";
    

  if ($context['sandbox']['counts']->error > 0)
  {
    $warning_msg  = "There was an error importing a few records -- ";
    $warning_msg .= $context['sandbox']['counts']->error . " to be exact";
    
    drupal_set_message(check_plain(t($warning_msg)), 'error');
  }
    
  //For attatchments, we need to flush the cache
  drupal_flush_all_caches();

  //Set an output message...
  foreach($out_message as $msg)
    drupal_set_message(check_plain($msg));
}



function _biblio_migrate_batch_process($import_mode, $ol, $input_file, &$context) {

  //Set execution time
  ini_set('max_execution_time', 0);

  //Get the tracking variable...
  $tracking_array = variable_get('biblio_migrate_tracking', array());
  
  //Load the filepath
  $filename = drupal_realpath($input_file->uri);
  
  //Get the number of lines
  $file_lines = get_file_linecount($filename);
  
  //Limit?
  $record_import_limit = ($ol > 0 && $ol < $file_lines) ? $ol : $file_lines;
  
  //Setup context
  $context['message'] = t("Importing $record_import_limit of $file_lines records in the file!");
  $context['finished'] = 0;
  
  //Open the file for reading
  $input_fh = fopen($filename, 'r');
    
  //Keep track of the ids that we added (not updated)
  $context['sandbox']['added_ids'] = array();
  
  $context['sandbox']['counts'] = new stdClass;
  $context['sandbox']['counts']->total = 0;
  $context['sandbox']['counts']->added = 0;
  $context['sandbox']['counts']->updated = 0;
  $context['sandbox']['counts']->deleted = 0;
  $context['sandbox']['counts']->error = 0;
 
  $curr_count = 0;
  
  while ($line = trim(fgets($input_fh)) && $curr_count < 5) {
        
    //Limit?
    if ($context['sandbox']['counts']->total == $record_import_limit)
    {
      $context['finished'] = 1;
      break;
    }
    
    //Valid JSON? Okay.  Work on this record...
    if ($record = json_decode($line, TRUE)) {
            
      $incoming_node_id = $record['nid'];

      //Skip those we've already done.
      if (in_array($incoming_node_id, $context['results']))
        continue;
      
      //Attatchment information?  Prepare to import that..
      if (isset($record['field_attatchment']) && ! empty($record['field_attatchment'])) {
        $attach_info = $record['field_attatchment']['und'][0];
      }

      $record = _biblio_migrate_node_prepare_for_import($record);
      
      //Node pre-prep
      $node = (object) $record;
      node_object_prepare($node);
      node_submit($node);

      //Check if updating
      if ($import_mode == 'update' && in_array($original_node_id, array_keys($tracking_array))) {
        
        $node->nid = $tracking_array[$incoming_node_id];
        
        //Get the original
        if ($orig_node = node_load($node->nid)) {
        
          //Get the node from the database, and also populate a few of the other important variables, ESPECIALLY VID
         
          foreach($orig_node as $k => $v) {
            if ( ! isset($node->$k))
              $node->$k = $v;
          }        
        
          $update = TRUE;
        } else {
          unset($tracking_array[$incoming_node_id]);
          $update = FALSE;
        }
      }
      else
        $update = FALSE;   

      //Save it!
      node_save($node);
            
      //Fix up the file attatchment
      if (isset($attach_info))
        _biblio_migrate_import_process_attachment($attach_info, $node->nid, $node->vid);

      //Record stats...
      //If updating.. see if the variable is in our tracking_array as a key & run the update function)
      if ($update) {
          $context['sandbox']['counts']->updated++;
      }
      else { //If Adding...
         $context['sandbox']['counts']->added++;
         $context['sandbox']['added_ids'][] = $node->nid; //@TODO: This gets replaced with the new node id
         
         //Add it to the tracking array //@TODO: Uncomment this, and of course, test it
         $tracking_array[$incoming_node_id] = $node->nid; 
      }      
    }
    else
      $context['sandbox']['counts']->error++;
    
    $context['sandbox']['counts']->total++;
    $curr_count++;
    
    //Inform the context of where we are
    $context['finished'] = $context['sandbox']['counts']->total / $record_import_limit;
    $context['results'][] = $incoming_node_id;
      
  }
  
  fclose($input_fh);
  
  //Update the tracking array
  variable_set('biblio_migrate_tracking', $tracking_array);
  
  //If import_mode is replace, then go through and delete any
  //biblio nodes with IDs that are not in the $added_ids array
  if ($import_mode == 'replace') {
    $result = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => 'biblio'));
    foreach ($result as $r) {
      if ( !in_array($r->nid, $context['sandbox']['added_ids'])) {
        node_delete($r->nid);
        $context['sandbox']['counts']->deleted++;
      }
    }
  }
  
}

// ---------------------------------------------------------

function biblio_migrate_import_form($form, &$form_state) {
  
  //Exeuction time warning
  $orig_exec_time = ini_get('max_execution_time');
  if ( ! ini_set('max_execution_time', 0)) {
    $msg = "Drupal was unable to reset the maximum execution time for
        a script on the server.  If you have a large collection, this action may
        fail.  The script will timeout after $orig_exec_time seconds";

    drupal_set_message(check_plain(t($msg)), 'warning');
  }
  else
    ini_set('max_execution_time', $orig_exec_time);

  $msg = "<h3>" . t('Import Biblio') . "</h3>";
  $msg .= "<p>" . t('Choose a valid JSON file that was exported by this interface on this or another Drupal site') . "</p>";
  $form['header'] = array(
    '#markup' => $msg
  );
    
  //Upload filesize warning
  $msg = "<p>" . t('The maximum filesize that you can upload to this server is:') . ' <strong>' . format_size(file_upload_max_size()) . "</strong></p>";
  $form['filesize_msg'] = array(
    '#markup' => $msg
  );

  //Actual input items
  $form['input_file'] = array(
    '#name' => 'files[import_file]',
    '#type' => 'managed_file',
    '#title' => t('Import file'),
    '#upload_location' => 'public://tmp_biblio_import/',
    '#progress_indicator' => 'bar',
    '#required' => TRUE
  );
  
  $form['import_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import Options')
  );
  
  $import_modes = array(
    'update' => t('Update biblio'),
    'replace' => t('Replace entire biblio'),
    'append' => t('Append to biblio')
  );
  
  $form['import_options']['import_mode'] = array(
   '#type' => 'radios',
   '#title' => t('Import Mode'),
   '#default_value' => 'update',
   '#options' => $import_modes,
   '#description' => t('Choose import logic for importing biblio'),
   '#required' => TRUE
  );  

  $form['import_limit'] = array(
    '#type' => 'textfield',
    '#title' => t("Limit import records"),
    '#description' => t('Limit input records (mainly used for testing).  0 is Unlimited'),
    '#required' => TRUE,
    '#default_value' => 0
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import It!')
  );

  return $form;
}

// ---------------------------------------------------------

function _biblio_migrate_import_process_attachment($attach_info, $nid, $vid) {
  
  $debug_msg[] = "File Attatchment Import for: $nid (version $vid)";

  //Does the $attach_info->uri exist in the database already?
    //If so, find the corresponding record attachment join tables, and delete them (if they exist)

  //No?
    //Run file_save to add it to the database

  //Create a record attatchment join tables, and update the node id
  $q = db_query("SELECT fid, uri FROM {file_managed} WHERE uri = :uri", array(':uri' => $attach_info['uri']));
    
  //Row exists in the database?
  if ($q->rowCount() > 0) {
    $fid = $q->fetchObject()->fid;
    $debug_msg[] = "Found file with uri {$attach_info['uri']} in the database.  Use existing fid: $fid"; 
  } else {
    unset($attach_info['fid']);
    unset($attach_info['uid']);
    $fid = file_save((object) $attach_info)->fid;
    $debug_msg[] = "No file with uri {$attach_info['uri']} in the database.  Created fid: $fid"; 
  }

  //Join tables for old fid exist?
  $q = db_query("SELECT * FROM {field_data_field_attatchment} WHERE field_attatchment_fid = :fid", array(':fid' => $fid));

  //Delete the old rows for the file attatchment
  if ($q->rowCount() > 0) {
    $num_deleted = db_delete('field_data_field_attatchment')->condition('field_attatchment_fid', $fid)->execute();
    $num_deleted = db_delete('field_revision_field_attatchment')->condition('field_attatchment_fid', $fid)->execute();
    $debug_msg[] = "Deleted existing file relationships for file with id $fid from the join tables";
  }

  //Add the new rows 
  $fields = array(
    'entity_type' => 'node',
    'bundle' => 'biblio',
    'entity_id' => $nid,
    'revision_id' => $vid,
    'language' => 'und',
    'delta' => 0,
    'field_attatchment_fid' => $fid
   );

  //Insert values
  db_insert('field_data_field_attatchment')->fields($fields)->execute();
  db_insert('field_revision_field_attatchment')->fields($fields)->execute();
  $debug_msg[] = "Created new file relationships for file with id $fid, node_id $nid, and version_id $vid";

  //drupal_set_message(implode("<br/>", $debug_msg));

  return TRUE;
}

// ---------------------------------------------------------

/**
 * Prepare exported object for import
 *
 * If this weren't hacked together, we would want to get the comments,
 * and we would definitely also want to read each custom field and see if
 * the field structure exists before attempting to import it
 * 
 * @param array $input_node
 * The raw JSON field from the original output
 * 
 * @return array
 * Something we can actually insert or update in the database
 */
function _biblio_migrate_node_prepare_for_import($input_node) {

  //Unset things we need to unset
  $to_unset = array(
    'nid', 'vid', 'cid', 'uid', 'uuid', 'log', //this will be managed automatically
    'tnid', 'created', 'changed', //ditto
    'revision_timestamp', 'revision_uid', //new node or existing, so kill the old stuff
    'comment', 'last_comment_timestamp', 'last_comment_name', 'last_comment_uid', 'comment_count', //unset comment stuff for now
    'rdf_mapping' //this ought to be regenerated automatically
  );

  foreach($to_unset as $us) {
    if (isset($input_node[$us]))
      unset($input_node[$us]);
  }
  
  //Convert basic biblio fields
  //Looks likes nothing to do
  
  //Convert biblio contributors
  //figure out how to add em
    //we'll need:
      //      'name' => 'Berlim, Marcelo T.',
      //      'auth_category' => '1',
      //      'auth_type' => '1',
      //      'rank' => '',
  foreach ($input_node['biblio_contributors'] as &$contrib) {
    
    unset($contrib['nid']);
    unset($contrib['vid']);
    unset($contrib['cid']);
    unset($contrib['drupal_uid']);
    unset($contrib['md5']);
  }


  if (isset($input_node['field_attatchment']))
    unset($input_node['field_attatchment']);

  //Return it
  return $input_node;
}

// ---------------------------------------------------------

function get_file_linecount($filename) {
  
  $linecount = 0;
  $handle = fopen($filename, "r");

  while(!feof($handle)){
    $line = trim(fgets($handle));
    
    if ($line != '')
      $linecount++;
  }

  fclose($handle);

  return $linecount;
}

/* EOF: biblio_migrate_export.inc */